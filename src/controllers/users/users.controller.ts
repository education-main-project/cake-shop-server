import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { take } from 'rxjs';
import { UserDto, UserLoginDTO, UserRegistrationDTO } from 'src/dto/UserDto';
import { ILSUser, IUserLogin, IUserRegistration } from 'src/interfaces/IUser';
import { JwtAuthGuard } from 'src/services/auth/jwt-auth.guard/jwt-auth.guard';
import { UsersService } from 'src/services/users/users.service';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @UseGuards(AuthGuard('user-guard'))
  @Get(':username')
  getUser(@Param('username') username: string): Promise<UserDto | null> {
    console.log(`search user "${username}"`);
    return new Promise<UserDto | null>((resp, rej) => {
      this.usersService
        .findUserByUserName(username)
        .then((user) => {
          this.usersService.$guardUserName
            .pipe(take(1))
            .subscribe(async (gusername) => {
              console.log(`guard user is "${gusername}"`);
              const guardUser =
                await this.usersService.findUserByUserName(gusername);
              if (guardUser) {
                if (guardUser.role !== 'admin' && username !== gusername) {
                  console.log('reject');
                  rej(
                    new HttpException(
                      [{ message: 'Действие запрещено' }],
                      HttpStatus.FORBIDDEN,
                    ),
                  );
                } else {
                  resp(new UserDto(user));
                }
              } else {
                console.log('Guard user not found');
                resp(null);
              }
            });
        })
        .catch((err) => {
          console.log(err);
          rej(
            new HttpException(
              [{ message: `Пользователь "${username}" - не найден.` }],
              HttpStatus.NOT_FOUND,
            ),
          );
        });
    });
  }
  @Get()
  getAllUsers(): string {
    return this.usersService.getAllUsers();
  }

  @Post()
  async addUser(@Body() data: IUserRegistration): Promise<ILSUser> {
    const user = new UserRegistrationDTO(data);
    const usersExists = await this.usersService.checkRegUser(
      user.username,
      user.email,
    );
    if (!usersExists.length) {
      try {
        console.log('try reg');
        const nUser = await this.usersService.addUser(user);
        return this.usersService.login(nUser.username, new UserLoginDTO(nUser));
      } catch (err) {
        if (typeof err.errors === 'object' && err.errors !== null) {
          const dt: { field: string; message: string }[] = [];
          const k = Object.keys(err.errors);
          for (const n in k) {
            dt.push({
              field: err.errors[k[n]].path,
              message: err.errors[k[n]].message,
            });
          }
          console.log(dt);
          throw new HttpException(dt, HttpStatus.BAD_REQUEST);
        } else {
          throw new HttpException(err.errors, HttpStatus.BAD_REQUEST);
        }
      }
    } else {
      if (usersExists[0].email === user.email) {
        throw new HttpException(
          [{ field: 'email', message: 'Почта уже используется' }],
          HttpStatus.CONFLICT,
        );
      } else {
        throw new HttpException(
          [
            {
              field: 'username',
              message: 'Пользователь с таким именем уже существует.',
            },
          ],
          HttpStatus.CONFLICT,
        );
      }
    }
  }
  @UseGuards(JwtAuthGuard)
  @Post('/check')
  checkToken(): Promise<{ ok: 'ok' }> {
    return new Promise((resolve) => {
      resolve({ ok: 'ok' });
    });
  }

  @UseGuards(AuthGuard('local'))
  @Post(':username')
  authUser(
    @Body() data: IUserLogin,
    @Param('username') username: string,
  ): Promise<ILSUser> {
    return this.usersService.login(username, data);
  }

  @UseGuards(AuthGuard('user-guard'))
  @Put(':id')
  updateUser(
    @Param('id') id: string,
    @Body() data: IUserRegistration,
  ): Promise<UserDto> {
    return this.usersService.updateUser(id, new UserRegistrationDTO(data));
  }
}
