import { Global, Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { AuthModule } from 'src/global/auth/auth.module';

@Global()
@Module({
  imports: [AuthModule],
  controllers: [UsersController],
})
export class UsersModule {}
