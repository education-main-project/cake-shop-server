import { Module } from '@nestjs/common';
import { CakesListController } from './cakes-list.controller';
import { ProductModelModule } from 'src/global/product-model/product-model.module';
import { CakeListService } from 'src/services/cake-list/cake-list.service';

@Module({
  imports: [ProductModelModule],
  controllers: [CakesListController],
  providers: [CakeListService],
})
export class CakesListModule {}
