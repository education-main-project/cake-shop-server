import { Test, TestingModule } from '@nestjs/testing';
import { CakesListController } from './cakes-list.controller';

describe('CakesListController', () => {
  let controller: CakesListController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CakesListController],
    }).compile();

    controller = module.get<CakesListController>(CakesListController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
