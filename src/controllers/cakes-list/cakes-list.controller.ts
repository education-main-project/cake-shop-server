import { CakeListService } from 'src/services/cake-list/cake-list.service';
import { Controller, Get, Param, Query } from '@nestjs/common';
import { ProductionDTO, ProductionDiscontDTO } from 'src/dto/ProductionDTO';
import { IPagination } from 'src/interfaces/IPagination';

@Controller('cakes-list')
export class CakesListController {
  constructor(private cakeListService: CakeListService) {}
  @Get('/recipes')
  recipesList(): Promise<ProductionDTO[]> {
    return this.cakeListService.recieptList();
  }

  @Get('/production/:pagesize/:first')
  productionList(
    @Param('pagesize') pagesize: number,
    @Param('first') first: number,
    @Query('classification') classification: string[],
  ): Promise<IPagination<ProductionDTO[]>> {
    console.log(
      `list: "first":"${first}",  "pagesize":"${pagesize}", "classification":"${classification}"`,
    );
    return this.cakeListService.productionList(
      first ? first : 0,
      pagesize ? pagesize : 0,
      classification ? classification : [],
    );
  }

  @Get('/production')
  getMany(@Query('ids') ids: string[] | undefined) {
    return this.cakeListService.productionGetMany(ids ? ids : []);
  }

  @Get('/production/discont')
  getDiskont(): Promise<ProductionDiscontDTO[]> {
    return this.cakeListService.getDiscont();
  }
}
