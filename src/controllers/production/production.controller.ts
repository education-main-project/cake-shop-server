import { diskStorage } from 'multer';
import { JwtAuthGuard } from './../../services/auth/jwt-auth.guard/jwt-auth.guard';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { IProduction, IShortProduction } from 'src/interfaces/IProduction';
import { ProductionService } from 'src/services/production/production.service';
import { IPagination } from 'src/interfaces/IPagination';
import { ProductionDTO } from 'src/dto/ProductionDTO';
import { Query } from '@nestjs/common';

@Controller('production')
export class ProductionController {
  constructor(private prodService: ProductionService) {}
  private static tempFileName: string = '';
  private static tempFileName2: string = '';
  @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'newFile', maxCount: 1 },
        { name: 'newFile2', maxCount: 1 },
      ],
      {
        storage: diskStorage({
          destination: './public/tmp',
          filename: (req, file, cd) => {
            console.log(file.fieldname);
            const imgType = file.mimetype.split('/');
            const uniqueSuffix =
              Date.now() + '-' + Math.round(Math.random() * 1e9);
            const imgName =
              file.fieldname + '-' + uniqueSuffix + '.' + imgType[1];
            cd(null, imgName);
            console.log('file get:', file.originalname);
            console.log('file proceed to:', imgName);
            if (file.fieldname === 'newFile') {
              ProductionController.tempFileName = imgName;
            } else {
              ProductionController.tempFileName2 = imgName;
            }
          },
        }),
      },
    ),
  )
  async createProduct(@Body() data: IShortProduction): Promise<IProduction> {
    try {
      const rVal = await this.prodService.createNew(
        data,
        ProductionController.tempFileName,
        ProductionController.tempFileName2,
      );
      ProductionController.tempFileName = '';
      ProductionController.tempFileName2 = '';
      return rVal;
    } catch (err) {
      if (typeof err.errors === 'object' && err.errors !== null) {
        const dt: { field: string; message: string }[] = [];
        const k = Object.keys(err.errors);
        for (const n in k) {
          dt.push({
            field: err.errors[k[n]].path,
            message: err.errors[k[n]].message,
          });
        }
        console.log(dt);
        throw new HttpException(dt, HttpStatus.BAD_REQUEST);
      } else {
        throw new HttpException(err.errors, HttpStatus.BAD_REQUEST);
      }
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get(':pagesize/:first/')
  list(
    @Param('pagesize') pagesize: number,
    @Param('first') first: number,
    @Query('classification') classificationFilter?: string,
    @Query('name') name?: string,
  ): Promise<IPagination<ProductionDTO[]>> {
    console.log(
      `list: "first":"${first}",  "pagesize":"${pagesize}", "classification":"${classificationFilter}", "name":"${name}"`,
    );
    return this.prodService.getList(
      first,
      pagesize,
      classificationFilter,
      name,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  getOne(@Param('id') id: string): Promise<ProductionDTO> {
    return this.prodService.getOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'newFile', maxCount: 1 },
        { name: 'newFile2', maxCount: 1 },
      ],
      {
        storage: diskStorage({
          destination: './public/tmp',
          filename: (req, file, cd) => {
            console.log(file.fieldname);
            const imgType = file.mimetype.split('/');
            const uniqueSuffix =
              Date.now() + '-' + Math.round(Math.random() * 1e9);
            const imgName =
              file.fieldname + '-' + uniqueSuffix + '.' + imgType[1];
            cd(null, imgName);
            console.log('file get:', file.originalname);
            console.log('file proceed to:', imgName);
            if (file.fieldname === 'newFile') {
              ProductionController.tempFileName = imgName;
            } else {
              ProductionController.tempFileName2 = imgName;
            }
          },
        }),
      },
    ),
  )
  updateProduct(@Param('id') id: string, @Body() data: IProduction) {
    const rVal = this.prodService.update(
      id,
      ProductionController.tempFileName,
      ProductionController.tempFileName2,
      data,
    );
    ProductionController.tempFileName = '';
    ProductionController.tempFileName2 = '';
    return rVal;
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  deleteMany(@Param('id') id: string) {
    return this.prodService.delete(id);
  }
}
