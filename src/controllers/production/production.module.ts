import { Module } from '@nestjs/common';
import { ProductionController } from './production.controller';
import { DbDataModule } from 'src/global/db-data/db-data.module';

@Module({
  imports: [DbDataModule],
  controllers: [ProductionController],
})
export class ProductionModule {}
