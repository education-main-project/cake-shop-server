import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from 'src/resource/constans/constants';
import { UsersService } from 'src/services/users/users.service';

@Injectable()
export class JwtStrategyService extends PassportStrategy(
  Strategy,
  'user-guard',
) {
  constructor(private userService: UsersService) {
    console.log('JwtStrategyService: init');
    console.log('JwtStrategyService secret: ' + jwtConstants.secret);
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: any): Promise<any> {
    console.log(`Validate "user-guard" - user: ""${payload.username}`);
    this.userService.setGuardUserName(payload.username);
    return { email: payload.sub, username: payload.username };
  }
}
