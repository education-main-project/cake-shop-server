import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { jwtConstants } from 'src/resource/constans/constants';
import { UsersService } from 'src/services/users/users.service';

@Injectable()
export class JwtAdminStrategyService extends PassportStrategy(Strategy) {
  constructor(private userService: UsersService) {
    console.log('JwtAdminStrategyService: init');
    console.log('JwtAdminStrategyService secret: ' + jwtConstants.secret);
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }
  async validate(payload: any): Promise<any> {
    const user = await this.userService.findUserByUserName(payload.username);
    if (user) {
      if (user.role === 'admin') {
        console.log(
          `JwtAdminStrategyService: пользователь "${payload.username}" - "admin", доступ разрешен.`,
        );
        return { email: payload.sub, username: payload.username };
      } else {
        console.log(
          `JwtAdminStrategyService: доступ запрещен ("${payload.username}" не "admin")`,
        );
        throw new HttpException(
          [{ message: 'Доступ запрещен' }],
          HttpStatus.FORBIDDEN,
        );
      }
    } else {
      console.log(
        `JwtAdminStrategyService: пользователь "${payload.username}" - не найден`,
      );
      throw new HttpException(
        [{ message: `Пользователь "${payload.username}" не найден.` }],
        HttpStatus.NOT_FOUND,
      );
    }
  }
}
