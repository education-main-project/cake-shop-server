import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';

@Injectable()
export class AuthService extends PassportStrategy(Strategy) {
  constructor(private usersService: UsersService) {
    super({ usernameField: 'username', passwordField: 'password' });
  }

  async validate(username: string, password: string): Promise<any> {
    const user = await this.usersService.checkAuthUser(username, password);
    console.log('AuthService', `"${username}"`, `"${password}"`, user);
    if (!user) {
      const errText = 'Неправильный логин или пароль.';
      throw new HttpException(
        [
          { field: 'username', message: errText },
          { field: 'password', message: errText },
        ],
        HttpStatus.UNAUTHORIZED,
      );
    }
    console.log('AuthService', 'ok');
    return true;
  }
}
