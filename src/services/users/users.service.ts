import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ShortUserDto, UserDto, UserRegistrationDTO } from 'src/dto/UserDto';
import { User, UserDocument } from 'src/schemas/UserSchema';
import { compare } from 'bcrypt';
import { ILSUser, IUser, IUserLogin } from 'src/interfaces/IUser';
import { JwtService } from '@nestjs/jwt';
import { BehaviorSubject } from 'rxjs';
import { hashSync, genSaltSync } from 'bcrypt';

@Injectable()
export class UsersService {
  private guardUserName = new BehaviorSubject<string>('');
  readonly $guardUserName = this.guardUserName.asObservable();
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    private jwtService: JwtService,
  ) {
    console.log('UsersService - Init');
  }

  setGuardUserName(username: string): void {
    this.guardUserName.next(username);
  }

  getAllUsers(): string {
    return 'All Users';
  }
  async addUser(data: UserRegistrationDTO): Promise<User> {
    const userData = new this.userModel(data);
    console.log(data);
    return userData.save();
  }
  async login(username: string, userData: IUserLogin): Promise<ILSUser> {
    console.log(
      `Login userData "${userData.username}"=>"${userData.password}"`,
    );
    const user = await this.userModel.findOne<IUser>({ username: username });
    if (user) {
      const payload = {
        username: username,
        sub: user.email,
      };
      return {
        access_token: this.jwtService.sign(payload),
        user: new ShortUserDto(user as IUser),
      };
    } else {
      throw new HttpException(
        [{ field: 'username', message: `Пользователь "${username}"` }],
        HttpStatus.BAD_REQUEST,
      );
    }
  }
  async findUserByUserName(usernam: string): Promise<User | null> {
    return await this.userModel.findOne({ username: usernam });
  }
  async checkRegUser(username: string, email: string): Promise<User[]> {
    console.log('checkRegUser:', username, email);
    return await this.userModel
      .find()
      .or([{ username: username }, { email: email }]);
  }
  async checkAuthUser(username: string, psw: string): Promise<User | null> {
    const user = await this.userModel.findOne({ username: username });
    console.log(`checkAuthUser("${username}", "${psw}"): `, user);
    if (!user) {
      return null;
    }
    if (!(await compare(psw, user.password))) {
      return null;
    }
    return user;
  }

  async updateUser(id: string, data: UserRegistrationDTO): Promise<UserDto> {
    const user = await this.userModel.findOne({ _id: id });
    return new Promise<UserDto>((res, rej) => {
      if (user) {
        if (data.password) {
          user.password = hashSync(data.password, genSaltSync());
        }
        user.username = data.username;
        user.realname = data.realname;
        user.email = data.email;
        user.role = data.role;
        user
          .save()
          .then((storeUser) => {
            res(new UserDto(storeUser));
          })
          .catch(rej);
      } else {
        rej(
          new HttpException(
            [{ message: 'Пользователь не найден' }],
            HttpStatus.NOT_FOUND,
          ),
        );
      }
    });
  }
}
