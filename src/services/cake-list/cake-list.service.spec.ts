import { Test, TestingModule } from '@nestjs/testing';
import { CakeListService } from './cake-list.service';

describe('CakeListService', () => {
  let service: CakeListService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CakeListService],
    }).compile();

    service = module.get<CakeListService>(CakeListService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
