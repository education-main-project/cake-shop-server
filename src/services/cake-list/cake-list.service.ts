import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ProductionDTO, ProductionDiscontDTO } from 'src/dto/ProductionDTO';
import { IPagination } from 'src/interfaces/IPagination';
import { Production, ProductionDocument } from 'src/schemas/production-schema';

@Injectable()
export class CakeListService {
  constructor(
    @InjectModel(Production.name)
    private productionModel: Model<ProductionDocument>,
  ) {}
  async recieptList(): Promise<ProductionDTO[]> {
    const answ = await this.productionModel.find({
      classification: 'торт',
      imgPath: { $ne: null, $nin: [''] },
      recipe: { $ne: null, $nin: [''] },
    });
    return answ.map((it) => new ProductionDTO(it));
  }

  async productionList(
    first: number,
    pageSize: number,
    classificationFilter?: string[] | undefined,
  ): Promise<IPagination<ProductionDTO[]>> {
    const count = await this.productionModel.countDocuments({
      classification: classificationFilter,
    });
    const _first = first < count ? first : 0;
    const tmpDT = await this.productionModel
      .find({ classification: classificationFilter })
      .limit(pageSize)
      .skip(_first);
    const rv: IPagination<ProductionDTO[]> = {
      data: tmpDT.map((it) => new ProductionDTO(it)),
      total: count,
      page: Math.floor(first / pageSize),
      first: _first,
      pageSize: pageSize,
    };
    return rv;
  }
  async productionGetMany(ids: string[]): Promise<ProductionDTO[]> {
    return (await this.productionModel.find({ _id: ids })).map(
      (it) => new ProductionDTO(it),
    );
  }

  async getDiscont(): Promise<ProductionDiscontDTO[]> {
    return (await this.productionModel.find({ diskont: { $ne: 0 } })).map(
      (it) => new ProductionDiscontDTO(it),
    );
  }
}
