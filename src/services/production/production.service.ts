import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { extname, resolve } from 'path';
import { ProductionDTO, ShortProductionDTO } from 'src/dto/ProductionDTO';
import { IProduction, IShortProduction } from 'src/interfaces/IProduction';
import { Production, ProductionDocument } from 'src/schemas/production-schema';
import { IPagination } from 'src/interfaces/IPagination';
import { copyFileSync, existsSync, unlinkSync } from 'node:fs';
import { ok } from 'node:assert';

interface IStoreHelpler {
  fieldName: keyof Production;
  value: string;
}

@Injectable()
export class ProductionService {
  constructor(
    @InjectModel(Production.name)
    private productionModel: Model<ProductionDocument>,
  ) {}
  createNew(
    data: IShortProduction,
    tmpImgPath: string,
    tmpImgPath2: string,
  ): Promise<IProduction> {
    const prodDto = new ShortProductionDTO(data);
    const prodModel = new this.productionModel(prodDto);
    return new Promise<IProduction>((res) => {
      prodModel.save().then(async (saved) => {
        console.log(saved._id);
        if (tmpImgPath) {
          this.proceedWidthFile(saved, tmpImgPath, tmpImgPath2)
            .then((dt) => {
              console.log('Продукт добавлен, ошибок нет.');
              res(dt);
            })
            .catch((dt) => {
              console.log('Продукт добавлен, имеются ошибки.');
              res(dt);
            });
        } else {
          console.log('Продукт добавлен, ошибок нет.');
          res(new ProductionDTO(saved));
        }
      });
    });
  }
  async update(
    id: string,
    tmpImgPath: string,
    tmpImgPath2: string,
    _newData: IProduction,
  ): Promise<IProduction> {
    await this.productionModel.updateOne(
      { _id: id },
      new ProductionDTO({
        ..._newData,
        ...{
          imgPath: _newData.backImgPath
            ? _newData.backImgPath
            : _newData.imgPath,
        },
      }),
    );
    const data = await this.productionModel.findOne({ _id: id });
    return new Promise<IProduction>((resol) => {
      if (data) {
        if (tmpImgPath || tmpImgPath2) {
          if (tmpImgPath && data.imgPath) {
            this.unlinkToUpdate(data.id, data.imgPath, 'main_pic');
          }
          if (tmpImgPath2 && data.imgPath2) {
            this.unlinkToUpdate(data.id, data.imgPath2, 'second_pic');
          }
          this.proceedWidthFile(data, tmpImgPath, tmpImgPath2)
            .then((dt) => {
              console.log('Продукт обновлён, файл обновлен ошибок нет.');
              resol(dt);
            })
            .catch((dt) => {
              console.log('Продукт обновлён, имеются ошибки осхранения файла.');
              resol(dt);
            });
        } else {
          console.log('Продукт обновлён, ошибок нет.');
          resol(new ProductionDTO(data));
        }
      } else {
        throw new HttpException(
          [{ message: 'Запись не найдена' }],
          HttpStatus.NOT_FOUND,
        );
      }
    });
  }

  getList(
    first: number,
    pageSize: number,
    classificationFilter?: string | undefined,
    name?: string | undefined,
  ): Promise<IPagination<ProductionDTO[]>> {
    let filterParam: { [s: string]: string | { [s: string]: string } } = {};
    console.log('return promise');
    if (pageSize < 2) {
      pageSize = 2;
    }
    if (classificationFilter) {
      filterParam = {
        ...filterParam,
        ...{ classification: classificationFilter },
      };
    }
    if (name) {
      filterParam = {
        ...filterParam,
        ...{ name: { $regex: `.*${name}.*`, $options: 'i' } },
      };
    }
    return new Promise<IPagination<ProductionDTO[]>>(async (resolve) => {
      const count = await this.productionModel.countDocuments(filterParam);
      const _first = first < count ? first : 0;
      const tmpDT = await this.productionModel
        .find(filterParam)
        .limit(pageSize)
        .skip(_first);
      const rv: IPagination<ProductionDTO[]> = {
        data: tmpDT.map((it) => new ProductionDTO(it)),
        total: count,
        page: Math.floor(first / pageSize),
        first: _first,
        pageSize: pageSize,
      };
      resolve(rv);
    });
  }

  getOne(id: string): Promise<ProductionDTO> {
    return new Promise<IProduction>((res, reject) => {
      this.productionModel
        .findOne({ _id: id })
        .then((dt) => {
          res(new ProductionDTO(dt));
        })
        .catch((err) => {
          console.log('Ошибка поиска:', err);
          reject(
            new HttpException(
              [{ message: 'Запись не найдена' }],
              HttpStatus.NOT_FOUND,
            ),
          );
        });
    });
  }
  async delete(id: string) {
    try {
      const rec = await this.productionModel.findOne({ _id: id });
      if (rec) {
        if (rec.imgPath) {
          this.unlinkToUpdate(rec._id.toString(), rec.imgPath, 'main_pic');
        }
        if (rec.imgPath2) {
          this.unlinkToUpdate(rec._id.toString(), rec.imgPath2, 'second_pic');
        }
      }
      const res = await this.productionModel.deleteOne({ _id: id });
      return {
        status: ok,
        deletedCount: res.deletedCount,
      };
    } catch (err) {
      return new HttpException(
        [{ message: 'Ошибка удаления' }],
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  private storeFile(
    id: string,
    filePath: string,
    perfix: string,
    fieldName: keyof Production,
  ): Promise<IStoreHelpler> {
    const srcPath = resolve('public', 'tmp', filePath);
    const fName = `${id}${extname(filePath)}`;
    const destPath = resolve('public', 'store', `${perfix}_${fName}`);
    const rVal: IStoreHelpler = {
      fieldName: fieldName,
      value: `/public/${perfix}_${fName}?${this.randStr(10)}`,
    };
    return new Promise<IStoreHelpler>((res) => {
      try {
        copyFileSync(srcPath, destPath);
        unlinkSync(srcPath);
        res(rVal);
      } catch (err) {
        console.log(`Ошибка сохранения файла "${srcPath}":`, err);
        this.unlinkIfExist(srcPath);
        this.unlinkIfExist(destPath);
        rVal.value = '';
        res(rVal);
      }
    });
  }
  private unlinkToUpdate(id: string, url: string, perfix: string): void {
    const urlExt = extname(url);
    const qPos = urlExt.indexOf('?');
    const ext = qPos > -1 ? urlExt.slice(0, qPos) : urlExt;
    this.unlinkIfExist(resolve('public', 'store', `${perfix}_${id}${ext}`));
  }
  private unlinkIfExist(fPath: string): void {
    if (fPath) {
      console.log('unlinkIfExist:');
      if (existsSync(fPath)) {
        try {
          unlinkSync(fPath);
          console.log(`Файл "${fPath}" успешно удален.`);
        } catch (err) {
          console.log(`Ошибка удаления файла "${fPath}": `, err);
        }
      } else {
        console.log(`Файл "${fPath}" не найден.`);
      }
    }
  }
  private proceedWidthFile(
    saved: ProductionDocument,
    tmpImgPath: string,
    tmpImgPath2: string,
  ): Promise<ProductionDTO> {
    //perfix "main_pic";
    const promises: Promise<IStoreHelpler>[] = [];
    if (tmpImgPath) {
      promises.push(
        this.storeFile(saved._id.toString(), tmpImgPath, 'main_pic', 'imgPath'),
      );
    }
    if (tmpImgPath2) {
      promises.push(
        this.storeFile(
          saved._id.toString(),
          tmpImgPath2,
          'second_pic',
          'imgPath2',
        ),
      );
    }
    return new Promise<ProductionDTO>(async (res) => {
      const dt = await Promise.all(promises);
      console.log('Сохранены файлы:', dt);
      dt.forEach((it) => {
        saved.set(it.fieldName, it.value);
      });
      saved
        .save()
        .then((saved2) => {
          res(new ProductionDTO(saved2));
        })
        .catch((err) => {
          console.log(`Ошибка сохранения записи id="${saved._id}":`, err);
          res(new ProductionDTO(saved));
        });
    });
  }
  private randStr(sumString: number): string {
    const symbolArr =
      '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    let randomString = '';
    for (let i = 0; i < sumString; i++) {
      const index = Math.floor(Math.random() * symbolArr.length);
      randomString += symbolArr[index];
    }
    return randomString;
  }
}
