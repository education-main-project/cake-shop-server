import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import {
  IShortProduction,
  TPoductClassification,
} from 'src/interfaces/IProduction';
import { Validators } from 'src/validators/Validators';

export type ProductionDocument = HydratedDocument<Production>;

@Schema()
export class Production implements IShortProduction {
  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  name: string;

  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  description: string;

  @Prop({ default: '' })
  recipe: string;

  @Prop({ default: '' })
  sloganPromotion: string;

  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  classification: TPoductClassification;

  @Prop({ default: '' })
  imgPath: string;

  @Prop({ default: '' })
  imgPath2: string;

  @Prop({ default: 0 })
  coast: number;

  @Prop({ default: 0 })
  diskont: number;

  @Prop({ default: 0.2 })
  weight: number;
}

export const ProductionSchema = SchemaFactory.createForClass(Production);
