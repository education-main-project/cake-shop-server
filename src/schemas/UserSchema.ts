import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { IUserRegistration, TRoles } from 'src/interfaces/IUser';
import { Validators } from 'src/validators/Validators';
import { hashSync, genSaltSync } from 'bcrypt';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User implements IUserRegistration {
  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  username: string;

  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  realname: string;

  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  password: string;

  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  email: string;

  @Prop({
    required: { message: (props) => Validators.required(props.path) },
  })
  role: TRoles;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre('save', function (next) {
  if (this.isNew) {
    console.log('Hashing password\n');
    this.password = hashSync(this.password, genSaltSync());
  }
  next();
});
