import { __ } from '@squareboat/nestjs-localization/dist/helpers';
export class Validators {
  static required(txt: string): string {
    return `Поле "${__(txt, 'ru')}" должно быть указано`;
  }
}
