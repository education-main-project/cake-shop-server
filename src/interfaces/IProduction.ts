/**
 * Классификация:
 *  торт, пироженное, дисерт, выпечка
 */
export type TPoductClassification =
  | 'торт'
  | 'пироженное'
  | 'десерт'
  | 'выпечка';
export interface IShortProduction {
  name: string;
  description: string;
  recipe: string;
  sloganPromotion: string;
  classification: TPoductClassification;
  imgPath: string;
  imgPath2: string;
  weight: number;
  coast: number;
  diskont: number;
  backImgPath?: string;
}
export interface IProduction extends IShortProduction {
  _id: string;
}

export interface IProductionDiscont
  extends Pick<
    IProduction,
    '_id' | 'name' | 'sloganPromotion' | 'imgPath' | 'coast' | 'diskont'
  > {}
