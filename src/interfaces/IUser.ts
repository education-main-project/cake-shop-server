export type TRoles = 'admin' | 'moder' | 'user';

export interface IUser {
  _id?: string;
  username: string;
  realname: string;
  role: TRoles;
  email: string;
}

export interface IUserRegistration extends IUser {
  password: string;
  passwordRepeat?: string;
}

export interface IShortUser
  extends Pick<IUser, 'realname' | 'username' | 'role' | 'email'> {}

export interface IUserLogin
  extends Pick<IUserRegistration, 'username' | 'password'> {}

export interface ILSUser {
  access_token: string;
  user: IShortUser;
}
