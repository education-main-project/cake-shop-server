export interface IPagination<DataType> {
  data: DataType;
  total: number;
  page: number;
  first: number;
  pageSize: number;
}
