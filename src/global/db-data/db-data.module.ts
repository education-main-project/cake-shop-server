import { Module } from '@nestjs/common';
import { JwtAdminStrategyService } from 'src/services/auth/jwt-strategy/jwt-admin-strategy.service';
import { ProductionService } from 'src/services/production/production.service';
import { AuthModule } from '../auth/auth.module';
import { ProductModelModule } from '../product-model/product-model.module';

@Module({
  imports: [AuthModule, ProductModelModule],
  providers: [JwtAdminStrategyService, ProductionService],
  exports: [JwtAdminStrategyService, ProductionService],
})
export class DbDataModule {}
