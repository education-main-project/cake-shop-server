import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Production, ProductionSchema } from 'src/schemas/production-schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Production.name, schema: ProductionSchema },
    ]),
  ],
  exports: [MongooseModule],
})
export class ProductModelModule {}
