import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { LocalizationModule } from '@squareboat/nestjs-localization';
import { jwtConstants } from 'src/resource/constans/constants';
import { User, UserSchema } from 'src/schemas/UserSchema';
import { AuthService } from 'src/services/auth/auth.service';
import { JwtStrategyService } from 'src/services/auth/jwt-strategy/jwt-strategy.service';
import { UsersService } from 'src/services/users/users.service';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '120m' },
    }),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    LocalizationModule.register({
      path: 'src/resource/lang/',
      fallbackLang: 'ru',
    }),
  ],
  providers: [AuthService, UsersService, JwtStrategyService],
  exports: [
    PassportModule,
    JwtModule,
    MongooseModule,
    LocalizationModule,
    AuthService,
    UsersService,
    JwtStrategyService,
  ],
})
export class AuthModule {}
