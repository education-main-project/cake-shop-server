import { IToDelete } from 'src/interfaces/IToDelete';

export class ToDeleteDTO implements IToDelete {
  ids: string[];
  constructor(data: IToDelete) {
    if (Array.isArray(data.ids)) {
      this.ids = data.ids.map((it) => `${it}`);
    } else {
      this.ids = [];
    }
  }
}
