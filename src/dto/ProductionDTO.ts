import { Production, ProductionDocument } from './../schemas/production-schema';
import {
  IProduction,
  IProductionDiscont,
  IShortProduction,
  TPoductClassification,
} from 'src/interfaces/IProduction';

export class ShortProductionDTO implements IShortProduction {
  name: string;
  description: string;
  recipe: string;
  sloganPromotion: string;
  classification: TPoductClassification;
  coast: number;
  imgPath: string;
  imgPath2: string;
  weight: number;
  diskont: number;

  constructor(data: IShortProduction) {
    if (data.name) this.name = data.name.trim();
    if (data.description) this.description = data.description.trim();
    this.classification = data.classification;
    this.coast = +data.coast;
    if (data.imgPath) {
      this.imgPath = data.imgPath.trim();
    }
    if (data.imgPath2) {
      this.imgPath2 = data.imgPath2.trim();
    }
    if (data.diskont) {
      this.diskont = data.diskont;
    } else {
      this.diskont = 0;
    }
    this.recipe = data.recipe?data.recipe.trim():'';
    this.sloganPromotion = data.sloganPromotion?data.sloganPromotion.trim():'';
    this.weight = typeof(data.weight)==='number'?data.weight:0.2;
  }
}

export class ProductionDTO extends ShortProductionDTO implements IProduction {
  _id: string;
  constructor(data: IProduction | ProductionDocument) {
    super(data);
    if (data._id) {
      this._id = data._id.toString();
    }
  }
}

export class ProductionDiscontDTO implements IProductionDiscont {
  _id: string;
  name: string;
  sloganPromotion: string;
  imgPath: string;
  coast: number;
  diskont: number;
  constructor( data: IProductionDiscont | Production ) {
    if ((<IProductionDiscont>data)._id){
      this._id = (<IProductionDiscont>data)._id
    }
    this.name = data.name;
    this.sloganPromotion = data.sloganPromotion;
    this.imgPath = data.imgPath;
    this.diskont = data.diskont;
    this.coast = data.coast;
  }
}
