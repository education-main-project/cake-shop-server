import { IShortUser } from './../interfaces/IUser';
import {
  IUser,
  IUserLogin,
  IUserRegistration,
  TRoles,
} from 'src/interfaces/IUser';
import { User } from 'src/schemas/UserSchema';

export class ShortUserDto implements IShortUser {
  username: string;
  realname: string;
  role: TRoles;
  email: string;
  constructor(data: IUser | User) {
    if (data.username) this.username = data.username.trim();
    if (data.realname) this.realname = data.realname.trim();
    if (data.email) this.email = data.email.trim();
    this.role = data.role;
  }
}

export class UserDto extends ShortUserDto implements IUser {
  _id?: string;
  email: string;
  constructor(data: IUser | User) {
    super(data);
    if (!(data instanceof User)) {
      this._id = data._id;
    }
    if (data.email) {
      this.email = data.email.trim();
    }
  }
}

export class UserFullInfoDTO
  extends UserDto
  implements Pick<IUserRegistration, 'email'>
{
  email: string;
  constructor(data: IUserRegistration | User) {
    super(data);
    if (data.email) this.email = data.email.trim();
  }
}
export class UserRegistrationDTO
  extends UserFullInfoDTO
  implements IUserRegistration
{
  password: string;
  passwordRepeat: string;
  constructor(data: IUserRegistration) {
    super(data);
    this.password = data.password;
    this.passwordRepeat = data.passwordRepeat;
  }
}
export class UserLoginDTO implements IUserLogin {
  username: string;
  password: string;
  constructor(data) {
    if (data.username) this.username = data.username.trim();
    this.password = data.password;
  }
}
