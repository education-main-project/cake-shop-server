import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './controllers/users/users.module';
import { CakesListModule } from './controllers/cakes-list/cakes-list.module';
import { ProductionModule } from './controllers/production/production.module';

@Module({
  imports: [
    UsersModule,
    CakesListModule,
    ProductionModule,
    MongooseModule.forRoot(
      'mongodb://cackeAdmin:12345cackeadmin@188.225.84.135:27017/cacke-shop?authMechanism=DEFAULT&authSource=cacke-shop',
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
